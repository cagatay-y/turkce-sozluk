use reqwest;
use serde::Deserialize;
use serde_json;

#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum SearchResult {
    Found(Vec<Entry>),
    Error { error: String },
}

#[derive(Deserialize, Debug)]
pub struct Entry {
    #[serde(rename = "madde")]
    pub entry: String,
    #[serde(rename = "lisan")]
    pub language: String,
    #[serde(rename = "telaffuz")]
    pub pronounciation: Option<String>,
    #[serde(rename = "anlamlarListe")]
    pub definitions: Vec<Definition>,
}

#[derive(Deserialize, Debug)]
pub struct Definition {
    #[serde(rename = "anlam")]
    pub definition: String,
}

pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Client {
        let client = reqwest::Client::new();
        Client { client }
    }

    pub fn search(&self, term: &str) -> SearchResult {
        let response_text = self
            .client
            .get("https://sozluk.gov.tr/gts")
            .query(&[("ara", term)])
            .send()
            .unwrap()
            .text()
            .unwrap();

        serde_json::from_str(&response_text).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize() {
        let example_json = r#"[
  {
    "madde_id": "21643",
    "kac": "0",
    "kelime_no": "21444",
    "cesit": "0",
    "anlam_gor": "0",
    "on_taki": null,
    "madde": "hala",
    "cesit_say": "0",
    "anlam_say": "1",
    "taki": null,
    "cogul_mu": "0",
    "ozel_mi": "0",
    "lisan_kodu": "11",
    "lisan": "Arap\u00e7a \u1e2b\u0101le",
    "telaffuz": "ha'la",
    "birlesikler": "hala k\u0131z\u0131, hala o\u011flu, halazade",
    "font": null,
    "madde_duz": "hala",
    "gosterim_tarihi": null,
    "anlamlarListe": [
      {
        "anlam_id": "37191",
        "madde_id": "21643",
        "anlam_sira": "1",
        "fiil": "0",
        "tipkes": "0",
        "anlam": "Baban\u0131n k\u0131z karde\u015fi, bibi",
        "gos": "0",
        "ozelliklerListe": [
          {
            "ozellik_id": "19",
            "tur": "3",
            "tam_adi": "isim",
            "kisa_adi": "a.",
            "ekno": "30"
          }
        ]
      }
    ]
  },
  {
    "madde_id": "21644",
    "kac": "0",
    "kelime_no": "21446",
    "cesit": "0",
    "anlam_gor": "0",
    "on_taki": null,
    "madde": "h\u00e2l\u00e2",
    "cesit_say": "1",
    "anlam_say": "1",
    "taki": null,
    "cogul_mu": "0",
    "ozel_mi": "0",
    "lisan_kodu": "11",
    "lisan": "Arap\u00e7a \u1e25\u0101l\u0101",
    "telaffuz": "ha:l\u00e2:",
    "birlesikler": null,
    "font": null,
    "madde_duz": "hala",
    "gosterim_tarihi": null,
    "anlamlarListe": [
      {
        "anlam_id": "37192",
        "madde_id": "21644",
        "anlam_sira": "1",
        "fiil": "0",
        "tipkes": "0",
        "anlam": "\u015eimdiye kadar, o zamana kadar, h\u00e2len, hen\u00fcz",
        "gos": "0",
        "orneklerListe": [
          {
            "ornek_id": "4569",
            "anlam_id": "37192",
            "ornek_sira": "1",
            "ornek": "Annesini yan\u0131na ald\u0131\u011f\u0131 g\u00fcnlerdeki mutsuzlu\u011fum h\u00e2l\u00e2 i\u00e7imi karart\u0131yor.",
            "kac": "1",
            "yazar_id": "148",
            "yazar": [
              {
                "yazar_id": "148",
                "tam_adi": "Erhan Bener",
                "kisa_adi": "E. Bener",
                "ekno": "317"
              }
            ]
          }
        ],
        "ozelliklerListe": [
          {
            "ozellik_id": "24",
            "tur": "3",
            "tam_adi": "zarf",
            "kisa_adi": "zf.",
            "ekno": "35"
          }
        ]
      }
    ],
    "atasozu": [
      {
        "madde_id": "21645",
        "madde": "h\u00e2l\u00e2 o masal"
      }
    ]
  }
]"#;

        let deserialized: Vec<Entry> = serde_json::from_str(&example_json).unwrap();
        println!("{:#?}", deserialized);
    }
}
