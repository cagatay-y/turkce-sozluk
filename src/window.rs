use crate::gts::Client;
use gtk::prelude::*;
use crate::gts::Entry;
use crate::gts::SearchResult::*;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
}

impl Window {
    pub fn new() -> Self {
        let builder =
            gtk::Builder::new_from_resource("/org/gnome/gitlab/cagatay_y/TurkceSozluk/window.ui");
        let widget: gtk::ApplicationWindow = builder
            .get_object("window")
            .expect("Failed to find the window object");
        let search_entry: gtk::SearchEntry = builder
            .get_object("search_entry")
            .expect("Failed to find the search entry object");
        let entries_buffer: gtk::TextBuffer = builder
            .get_object("entries_buffer")
            .expect("Failed to find the entries buffer object");

        entries_buffer.get_tag_table().unwrap().foreach(|tag| println!("{}", tag.get_property_name().unwrap()));

        let client = Client::new();
        search_entry.connect_activate(move |entry| {
            let search_result = client.search(entry.get_text().unwrap().as_str());
            entries_buffer.set_text("");
            let mut start_iter = entries_buffer.get_start_iter();
            entries_buffer.insert_markup(
                &mut start_iter,
                &match search_result {
                    Found(entries) => marked_up_entries(&entries),
                    Error {error} => error
                }
            );
        });

        Self { widget }
    }
}

    fn marked_up_entries(entries: &Vec<Entry>) -> String {
        let mut mark_up = String::new();
        for entry in entries.iter() {
            mark_up.push_str(&format!("<span size='x-large'>{}</span>", entry.entry));
            if let Some(pronounciation) = &entry.pronounciation {
                mark_up.push_str(&format!(" <span style='italic'>{}</span>", pronounciation));
            }
            mark_up.push('\n');
            if entry.language != "" {
                mark_up.push_str(&format!("<span color='gray'>{}</span>\n", entry.language));
            }
            for (place, definition) in entry.definitions.iter().enumerate() {
                mark_up.push_str(&format!("<span weight='light'>{}.</span> {}\n", place+1, definition.definition));
            }
            mark_up.push('\n');
        }
        mark_up
    }
